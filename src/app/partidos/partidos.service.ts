import { Injectable } from '@angular/core';
import {games, events} from './partidos.model';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class PartidosService {

  private events: events[] = [
    {
      event_id: 1,
      game_id:100,
      time: 1,
      description: "Game started",
      team: "Real Madrid"
    }
  ]

  private games: games[] = [
    {
      id: 100,
      home_team: "Real Madrid",
      visitor: "Barcelona",
      date: "12-12-2020",
      score_home: 1,
      score_visitor: 0,
      events: this.events 
    },
    {
      id: 101,
      home_team: "Liga",
      visitor: "Saprissa",
      date: "12-19-2020",
      score_home: 0,
      score_visitor: 0,
      events: this.events 
    },
    {
      id: 102,
      home_team: "Atl",
      visitor: "Mad",
      date: "12-19-2020",
      score_home: 0,
      score_visitor: 0,
      events: this.events 
    }
  ]

  constructor(private storage: Storage) { }

    addGame(
    id: number,
    home_team: string,
    visitor: string,
    date: string,
    score_home: number,
    score_visitor: number,
    events: events[], 
  ) {
    const games: games = {
      id: id,
      home_team: home_team,
      visitor: visitor,
      date: date,
      score_home: score_home,
      score_visitor: score_visitor,
      events: this.events, 
    }
    this.games.push(games);
    this.storage.set(games.date, games);
  }

  editGame(
    id: number,
    home_team: string,
    visitor: string,
    date: string,
    score_home: number,
    score_visitor: number,
    events: string, 
  ) {
    let i = this.games.map((edit) => edit.id).indexOf(id);

    this.games[i].id = id;
    this.games[i].home_team = home_team;
    this.games[i].visitor = visitor;
    this.games[i].date = date;
    this.games[i].score_home = score_home;
    this.games[i].score_visitor = score_visitor;
    this.games[i].events = this.events;
  }

  getAll() {
    return [...this.games];
  }

  getGame(game_id: number) {
    return {
      ...this.games.find(
        games => {
          return games.id == game_id;
        }
      )
    };
  }

  finalize(game_id: number){
    return {
      ...this.games.find(
        games => {games.id === game_id;}
      )
    };
  }

  delete(game_id: number) {
    this.games = this.games.filter(
      games => {
        return games.id !== game_id;
      }
    );
  }

  addEvent(
    game_id: number,
    event_id: number,
    time: number,
    description: string,
    team: string,
  ) {
    const events: events = {
      event_id: event_id,
      game_id: game_id,
      time: time,
      description: description,
      team: team
    }

    if (events.description == "Goal") {
      let i = this.games.map((e) => e.id).indexOf(game_id); 

      if (this.games[i].home_team == events.team) {
        this.games[i].score_home++;
      } else {
        this.games[i].score_visitor++;
      }
    }
    this.events.push(events);
  }
}
