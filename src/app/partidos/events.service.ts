import { Injectable } from '@angular/core';
import {events} from './partidos.model';
@Injectable({
  providedIn: 'root'
})
export class EventsService {
  events: events[] = [
    {
      event_id: 1,
      game_id: null,
      time: 1,
      description: "Game started",
      team: ""
    },
    {
      event_id: 2,
      game_id: null,
      time: 90,
      description: "Game finished",
      team: ""
    }
  ]

  compareWith(o1: events, o2: events) {
    return o1 && o2 ? o1.event_id === o2.event_id : o1 === o2;
  }

  addEvent(
    event_id: number,
    game_id: number,
    time: number,
    description: string,
    team: string
  ) {
    const event: events = {
      event_id: event_id,
      game_id: game_id,
      time: time,
      description: description,
      team: team,
    }
    this.events.push(event);
  }

  editEvent(
    event_id: number,
    game_id: number,
    time: number,
    description: string,
    team: string,
  ) {
    let i = this.events.map((edit) => edit.event_id).indexOf(event_id);

    this.events[i].event_id = event_id;
    this.events[i].game_id = game_id;
    this.events[i].time = time;
    this.events[i].description = description;
    this.events[i].team = team;
  }

  constructor() { }
}
