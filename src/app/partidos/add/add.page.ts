import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PartidosService } from '../partidos.service';
import { EventsService } from '../events.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  formGameAdd: FormGroup;

  constructor(
    private PartidosService: PartidosService,
    private router: Router) {  
  }

  ngOnInit() {
    this.formGameAdd = new FormGroup({
      id: new FormControl(
        1,
        {
          updateOn: 'blur',
          validators: [Validators.required, Validators.min(1)]
        }
      ),
      home_team: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators: [Validators.required]
        }
      ),
      visitor: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators: [Validators.required]
        }
      ),
      date: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators: [Validators.required]
        }
      ),
      score_home: new FormControl(
        0,
        {
          updateOn: 'blur',
          validators: [Validators.required, Validators.minLength(1)]
        }
      ),
      score_visitor: new FormControl(
        0,
        {
          updateOn: 'blur',
          validators: [Validators.required, Validators.min(1)]
        }
      ),
      events: new FormControl(
        null,
        {
          updateOn: 'blur',
          validators: []
        }
      ),
    });
  }
  

  addGame(){
    if (!this.formGameAdd.valid) {
      return;
    }
    this.PartidosService.addGame(
      this.formGameAdd.value.id,
      this.formGameAdd.value.home_team,
      this.formGameAdd.value.visitor,
      this.formGameAdd.value.date,
      this.formGameAdd.value.score_home,
      this.formGameAdd.value.score_visitor,
      this.formGameAdd.value.events
    );
    this.formGameAdd.reset();
    this.router.navigate(['/partidos']);
  }

}
