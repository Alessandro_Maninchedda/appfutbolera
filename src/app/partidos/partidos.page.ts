import { Component, OnInit , Input} from '@angular/core';
import { games } from './partidos.model';
import { Router } from "@angular/router";
import { PartidosService } from './partidos.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-partidos',
  templateUrl: './partidos.page.html',
  styleUrls: ['./partidos.page.scss'],
})
export class PartidosPage implements OnInit {
  @Input() 
  id: number;
  games: games[];

  constructor (private PartidosService: PartidosService,private router: Router) { }


  ngOnInit() {
  }
  
  update(id: number) {
    this.router.navigate(["/partidos/edit/" + id]);
  }

  ionViewWillEnter() {
    // this.storage.games = this.PartidosService.getAll();
    this.games = this.PartidosService.getAll();
  }

  

}
