import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { PartidosService } from "../partidos.service";
import { games } from "../partidos.model";

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {
  formEventAdd: FormGroup;
  PartidosService: any;
  game_id: number;
  games: games;
  

  constructor(
    private router: Router,
    private activeRouter: ActivatedRoute,
    private servicePartidos: PartidosService,
  ) { }

  ngOnInit() {
    this.formEventAdd = new FormGroup({
      game_id: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      time: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      description: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      team: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });

    this.activeRouter.paramMap.subscribe((paramMap) => {
      if (!paramMap.has("game_id")) {
        return;
      }
      const game_id = parseInt(paramMap.get("game_id"));
      this.games = this.PartidosService.getPartido(game_id);
      this.game_id = game_id;
    });
  }

  addEvento() {
    if (!this.formEventAdd.valid) {
      return;
    }
    this.servicePartidos.addEvent(
      this.formEventAdd.value.event_id,
      this.formEventAdd.value.game_id = this.game_id,
      this.formEventAdd.value.time,
      this.formEventAdd.value.description,
      this.formEventAdd.value.team
    );
    this.formEventAdd.reset();
    this.router.navigate(["/partidos"]);
  }

}
