import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { PartidosService } from '../partidos.service';
import { games, events } from '../partidos.model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})

export class DetailPage implements OnInit {
    game_id: number;
    games: games;
    

    constructor(
      private activeRouter: ActivatedRoute,
      private PartidosService: PartidosService,
      private router: Router,
      private alertController: AlertController
    ){}
    

  ngOnInit() {
    this.activeRouter.paramMap.subscribe(
      paramMap => {
        const game_id =  parseInt( paramMap.get('game_id'));
        this.games = this.PartidosService.getGame(game_id);
      }
    );
  }

  delete(){
    this.alertController.create({
      header: "Erase match-up",
      message: "Are you sure you wish to delete this game?",
      buttons:[
        {
          text:"No",
          role: 'no'
        },
        {
          text: 'Delete',
          handler: () => {
            this.PartidosService.delete(this.games.id);
            this.router.navigate(['./partidos']);
          }
        } 
      ]
    })
    .then(
      alertEl => {
        alertEl.present();
      }
    );
    
  }

  finalize(){
    this.alertController.create({
      header: "Erase match-up",
      message: "Are you sure you wish to finalize this game?",
      buttons:[
        {
          text:"No",
          role: 'no'
        },
        {
          text: 'Finalize',
          handler: () => {
            this.PartidosService.finalize(this.games.id);
            this.router.navigate(['./partidos']);
          }
        } 
      ]
    })
    .then(
      Alert => {
        Alert.present();
      }
    );
    
  }

  addEvent(id: number){
    this.router.navigate(["/games/events/" + id]);
  }

}

