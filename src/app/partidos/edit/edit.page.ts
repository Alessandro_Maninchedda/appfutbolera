import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { PartidosService } from "../partidos.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { games, events } from '../partidos.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  games: games;
  formGameEdit: FormGroup;

  constructor(
    private activeRouter: ActivatedRoute,
    private PartidosService: PartidosService,
    private router: Router
    ) { }

    ngOnInit() {
      this.activeRouter.paramMap.subscribe(
        paramMap => {
          const game_id =  parseInt( paramMap.get('game_id'));
          this.games = this.PartidosService.getGame(game_id);
        }
      );
    
      this.formGameEdit = new FormGroup({
        id: new FormControl(
          {
            updateOn: 'blur',
            validators: [Validators.required, Validators.min(1)]
          }
        ),
        home_team: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators: [Validators.required]
          }
        ),
        visitor: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators: [Validators.required]
          }
        ),
        date: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators: [Validators.required]
          }
        ),
        score_home: new FormControl(
          0,
          {
            updateOn: 'blur',
            validators: [Validators.required, Validators.minLength(1)]
          }
        ),
        score_visitor: new FormControl(
          0,
          {
            updateOn: 'blur',
            validators: [Validators.required, Validators.min(1)]
          }
        ),
        events: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators: []
          }
        ),
      });

      this.formGameEdit.value.id = this.games.id;
    }

    editGame(){
      if (!this.formGameEdit.valid) {
        return;
      }
      this.PartidosService.editGame(
        this.formGameEdit.value.id,
        this.formGameEdit.value.home_team,
        this.formGameEdit.value.visitor,
        this.formGameEdit.value.date,
        this.formGameEdit.value.score_home,
        this.formGameEdit.value.score_visitor,
        this.formGameEdit.value.events
      );
      this.formGameEdit.reset();
      this.router.navigate(['/partidos']);
    }
}
