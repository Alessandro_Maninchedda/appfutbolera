export interface games{
   id: number;
   home_team: string;
   visitor: string;
   date: string;
   score_home: number;
   score_visitor: number;
   events: events[]; 
}

export interface events{
   event_id: number;
   game_id: number;
   time: number;
   description: string;
   team: string;
}
